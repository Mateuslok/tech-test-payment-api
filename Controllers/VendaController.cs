using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;
        public VendaController(VendaContext context)
        {
            _context = context;
        }
        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
                return NotFound();

            return Ok(venda);

        }
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            Venda vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
            {
                return NotFound("Venda não localizada.");
            }

            if (((int)venda.Status) > 4)
            {
                return BadRequest("Status inválido.");
            }

            if (VerificaStatus(vendaBanco.Status, venda.Status).Equals(vendaBanco.Status))
            {
                return BadRequest($"Status {vendaBanco.Status} não pode ser atualizado para {venda.Status}.");
            }

            vendaBanco.Status = venda.Status;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok();
        }

        private EnumStatusVenda VerificaStatus(EnumStatusVenda statusAtual, EnumStatusVenda statusNovo)
        {
            if (statusAtual.Equals(EnumStatusVenda.AguardandoPagamento) &&
                (statusNovo.Equals(EnumStatusVenda.PagamentoAprovado) || statusNovo.Equals(EnumStatusVenda.Cancelado)))
            {
                return statusNovo;
            }

            if (statusAtual.Equals(EnumStatusVenda.PagamentoAprovado) &&
                statusNovo.Equals(EnumStatusVenda.EnviadoParaTransportadora) || statusNovo.Equals(EnumStatusVenda.Cancelado))
            {
                return statusNovo;
            }

            if (statusAtual.Equals(EnumStatusVenda.EnviadoParaTransportadora) &&
                statusNovo.Equals(EnumStatusVenda.Entregue))
            {
                return statusNovo;
            }

            return statusAtual;


        }

    }
}
